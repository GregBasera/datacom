import java.io.*;
import java.net.*;
import java.util.*;

public class Client {
  public static void main(String[] fudge) throws Exception{
    System.out.println("Connecting to Server");

    // ServerSocket server = new ServerSocket(54001);
    // Socket socket = server.accept();
    Socket socket = new Socket("localhost", 54123);

    BufferedReader sin = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    PrintWriter sout = new PrintWriter(socket.getOutputStream(), true);

    Scanner cin = new Scanner(System.in);

    while(true){
      String msg = null;

      // recieve
      while((msg = sin.readLine()) == null){}
      System.out.println("Client: " + msg);

      // send
      System.out.print("Input message: ");
      msg = cin.nextLine();
      sout.println(msg);
    }

    // socket.close();
  }
}
