import java.io.*;
import java.net.*;
import java.util.*;

public class Server {
  public static void main(String[] fudge) throws Exception{
    System.out.println("Starting Chat Server");

    ServerSocket server = new ServerSocket(54123);
    Socket socket = server.accept();

    BufferedReader sin = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    PrintWriter sout = new PrintWriter(socket.getOutputStream(), true);

    Scanner cin = new Scanner(System.in);

    while(true){
      String msg = null;

      // send
      System.out.print("Input message: ");
      msg = cin.nextLine();
      sout.println(msg);

      // recieve
      while((msg = sin.readLine()) == null){}
      System.out.println("Client: " + msg);
    }

    // socket.close();
  }
}
