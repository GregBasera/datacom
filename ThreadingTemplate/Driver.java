public class Driver{
  public static void main(String[] fudge){
    Name n1 = new Name("he");
    Name n2 = new Name("llo");
    Name n3 = new Name("there");

    Thread th1 = new Thread(n1);
    Thread th2 = new Thread(n2);
    Thread th3 = new Thread(n3);

    th1.start();
    th2.start();
    th3.start();
  }
}
