// Chat.java

import java.io.*; //BufferedReader, PrintWriter, InputStreamReader
import javax.swing.*;
import java.awt.event.*;

public class Chat extends JFrame implements ActionListener {
  private JPanel panel;
  private JLabel chatLabel, inputLabel;
  public JTextArea chatBox;
  private JScrollPane scroll;
  private JTextField inputBox;
  private JButton sendButton;
  private String username;
  private PrintWriter sout;

  public Chat(String username, PrintWriter sout)
  {
    this.username = username;
    this.sout = sout;

    setTitle("LabChat [" + username + "]");
    setSize(300, 300);
    setDefaultCloseOperation(EXIT_ON_CLOSE);

    panel = new JPanel();
    inputBox = new JTextField(25);
    chatBox = new JTextArea(10, 25);
      chatBox.setEditable(false);
    scroll = new JScrollPane (chatBox, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    chatLabel = new JLabel("Chatroom");
    inputLabel = new JLabel("Input");
    sendButton = new JButton("Send");

    sendButton.addActionListener(this);
    inputBox.addActionListener(this);

    panel.add(chatLabel);
    panel.add(scroll);
    panel.add(inputLabel);
    panel.add(inputBox);
    panel.add(sendButton);

    // panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
    add(panel);
    setVisible(true);
    setResizable(false);
  }

  // when the send button is clicked
  public void actionPerformed(ActionEvent ae){
    // Get the text from the inputBox
    String e = inputBox.getText();

    if(e.length() > 0){
      // Set text of inputBox to nothing for intuitivity
      inputBox.setText("");
      // Append username to the message and then send to text through the socket
      sout.println(username + ": " + e);
    } else
      JOptionPane.showMessageDialog(null, "Error, please enter message.");
  }
}
