// Receiver.java

import java.io.*; //BufferedReader, PrintWriter, InputStreamReader

public class Receiver implements Runnable{
	private BufferedReader sin;
	private Chat w;

	// Constructor
	public Receiver(BufferedReader sin, Chat w){
		this.sin = sin;
		this.w = w;
	}

	public void run(){
		String msg = null;

		try{
			// this thread runs forever
			while(true){
				// I think this is bruteforce and should never be done.
        while((msg = sin.readLine()) == null){} // but I can't find a way to replace it :(

				// Everytime a message is recieved through the socket, append the message
				// on the text already in the JTextArea in the GUI
				w.chatBox.append("\n" + msg);
			}
		}catch(IOException e){
			System.out.println("IOException in Receiver.java...");
		}
	}
}
