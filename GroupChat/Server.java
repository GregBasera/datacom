// Server.java

import java.io.*; //BufferedReader, PrintWriter, InputStreamReader
import java.net.*; //Socket, ServerSocket
import java.util.ArrayList;

public class Server {
	public static void main(String[] fudge) throws Exception {
		if(fudge.length < 1) {
			System.out.println("Usage: java Server [port]");
			System.exit(1);
		}

		// ArrayList of souts of all created client thread
		// The beauty of pass-by-refference :)
		// This ArrayList gets passed to ServerTick.java everytime a Client
		// connects.
		// The ServerTick then loops through this ArrayList and relay data to
		// each client everytime another client sends a message
		ArrayList<PrintWriter> souts = new ArrayList<PrintWriter>();

		// Creating a ServerSocket and binding it to the port number
		ServerSocket server = new ServerSocket(Integer.parseInt(fudge[0]));

		while(true){
			// Accepting clients to the initialized port number
			// It's in a infinite loop so this never stops accepting
			// unless the proccess is terminated
			Socket socket = server.accept();

			// Creating input and output streams for every Client accepted
			BufferedReader sin = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter sout = new PrintWriter(socket.getOutputStream(), true);

			// the output streams created are stored in the ArrayList
			souts.add(sout);

			// Initializing ServerTick Thread
			// Every Thread is constructed with its own input stream and an
			// ArrayList of output stream of all clients in the server
			ServerTick tick = new ServerTick(sin, souts);
			Thread tik = new Thread(tick);
			tik.start();

			System.out.println("Server running...");

			//socket.close();
		}
	}
}
