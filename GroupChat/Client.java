// Client.java

import java.io.*; //BufferedReader, PrintWriter, InputStreamReader
import java.net.*; //Socket, ServerSocket

public class Client{
	public static void main(String[] fudge) throws Exception {
		if(fudge.length < 3) {
			// Command Line Arguments shoulb be: [localhost | ip] [port] [Username]
			System.out.println("Usage: java Client [localhost | ip] [port] [Username]");
			System.exit(1);
		}

		System.out.println("Connecting to server...");

		Socket socket = new Socket(fudge[0], Integer.parseInt(fudge[1])); // Connect to Server
		String username = fudge[2]; // Initialize Username

		System.out.println("Connected...");

		// We need to declare these here because these are the objects that read and write from the actual
		// Sockets. Since we've declared the Socket variables here...
		BufferedReader sin = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		PrintWriter sout = new PrintWriter(socket.getOutputStream(), true);


		// I completely replaced Sender.java with Chat.java
		Chat chat = new Chat(fudge[2], sout);

		// Running Thread for Receiving data
		Receiver r = new Receiver(sin, chat);
		Thread receiveThread = new Thread(r);
		receiveThread.start();

		//socket.close();
	}
}
