// ServerTick.java

import java.io.*; //BufferedReader, PrintWriter, InputStreamReader
import java.util.ArrayList;

public class ServerTick implements Runnable {
  private BufferedReader sin;
  private ArrayList<PrintWriter> souts;

  // Constructor
  public ServerTick(BufferedReader sin, ArrayList<PrintWriter> souts){
    this.sin = sin;
    this.souts = souts;
  }

  public void run(){
    // this thread runs forever
    while(true){
      try{
        String msg = null;

        // I think this is bruteforce and should never be done.
        while((msg = sin.readLine()) == null){} // but I can't find a way to replace it :(

        System.out.println(msg);

        // Everytime this client sends a message, loop through the ArrayList
        // and relay the message repeatedly to each client in the server including itself
        for(int q = 0; q < souts.size(); q++){
          souts.get(q).println(msg);
        }
      } catch(IOException e) {
        System.out.println("IOException in ServerTick.java...");
      }
    }
  }
}
