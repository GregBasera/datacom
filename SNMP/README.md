# Requirements
---
I, the author, would advice anyone who plan to use this software to run it in a Linux system (Ubuntu) with **sudo** capabilities. Below are the nessesary applications the program will access to properly function.

| Linux Commands | Install |
|---|---|
| whoami | installed by default |
| uptime | installed by default |
| ifconfig | sudo apt get install ifconfig |
| iostat | sudo apt get install iostat |
| sensors | sudo apt get install lm-sensors |
