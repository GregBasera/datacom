import java.io.*; //BufferedReader, PrintWriter, InputStreamReader
import java.util.*; //Scanner

public class Receiver implements Runnable{
	private BufferedReader sin;

	public Receiver(BufferedReader sin){
		this.sin = sin;
	}

	public void run(){
		String msg;

		try{
			while(true){
				while((msg = sin.readLine()) == null){}
				System.out.print("\n" + msg + "\n> ");
			}
		}catch(Exception e){}
	}
}
