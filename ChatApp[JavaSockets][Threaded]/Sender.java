import java.io.*; //BufferedReader, PrintWriter, InputStreamReader
import java.util.*; //Scanner

public class Sender implements Runnable{
	private PrintWriter sout;
	private String username;

	public Sender(PrintWriter sout, String username){
		this.sout = sout;
		this.username = username;
	}

	public void run(){
		Scanner cin = new Scanner(System.in);

		while(true){
			System.out.print("> ");
			String msg = cin.next();

			sout.println(username + ": " + msg);
		}
	}
}
