import java.io.*; //BufferedReader, PrintWriter, InputStreamReader
import java.net.*; //Socket, ServerSocket
import java.util.*; //Scanner

public class Client{
	public static void main(String args[]) throws Exception{
		System.out.println("Connecting to server...");

		String username = "John";

		Socket socket = new Socket("localhost", 54123);

		System.out.println("Connected...");

		BufferedReader sin = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		PrintWriter sout = new PrintWriter(socket.getOutputStream(), true);


		Receiver r = new Receiver(sin);
		Sender s = new Sender(sout, username);

		Thread sendThread = new Thread(s);
		Thread receiveThread = new Thread(r);

		sendThread.start();
		receiveThread.start();

		//socket.close();
	}
}
