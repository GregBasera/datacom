import java.io.*; //BufferedReader, PrintWriter, InputStreamReader
import java.net.*; //Socket, ServerSocket
import java.util.*; //Scanner

public class Server{
	public static void main(String args[]) throws Exception{
		System.out.println("Starting Chat Server...");

		String username = "John";

		ServerSocket server = new ServerSocket(54123);
		Socket socket = server.accept();

		System.out.println("Connected...");

		BufferedReader sin = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		PrintWriter sout = new PrintWriter(socket.getOutputStream(), true);


		Receiver r = new Receiver(sin);
		Sender s = new Sender(sout, username);

		Thread sendThread = new Thread(s);
		Thread receiveThread = new Thread(r);

		sendThread.start();
		receiveThread.start();

		//socket.close();
	}
}
