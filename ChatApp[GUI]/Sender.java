import java.io.*; //BufferedReader, PrintWriter, InputStreamReader
import java.util.*; //Scanner

public class Sender implements Runnable{
	private PrintWriter sout;
	public String username;
	private Chat w;

	public Sender(PrintWriter sout, String username, Chat w){
		this.sout = sout;
		this.username = username;
		this.w = w;
	}

	public void sendMessage(String e){
		sout.println(e);
	}

	public void run(){
		Scanner cin = new Scanner(System.in);

		while(true){
			System.out.print("> ");
			String msg = cin.nextLine();

			String e = w.chatBox.getText();
			e = e + "\n" + msg;
			w.chatBox.setText(e);

			sout.println(username + ": " + msg);
		}
	}
}
