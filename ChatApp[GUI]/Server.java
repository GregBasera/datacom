import java.io.*; //BufferedReader, PrintWriter, InputStreamReader
import java.net.*; //Socket, ServerSocket
import java.util.*; //Scanner

public class Server{
	public static void main(String[] fudge) throws Exception{
		System.out.println("Starting Chat Server...");

		String username = "Gweg";

		ServerSocket server = new ServerSocket(Integer.parseInt(fudge[0]));
		Socket socket = server.accept();

		System.out.println("Connected...");

		Chat chat = new Chat();

		BufferedReader sin = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		PrintWriter sout = new PrintWriter(socket.getOutputStream(), true);

		Receiver r = new Receiver(sin, chat);
		Sender s = new Sender(sout, username, chat);

		chat.sender = s;

		Thread sendThread = new Thread(s);
		Thread receiveThread = new Thread(r);

		sendThread.start();
		receiveThread.start();

		//socket.close();
	}
}
