import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class Chat extends JFrame implements ActionListener {
  public JButton sendButton;
  public JLabel chatLabel, inputLabel;
  public JTextArea chatBox;
  public JTextField inputBox;
  public JPanel panel;
  public Sender sender;

  public Chat(){
    setTitle("LabChat");
    setSize(300, 300);
    setDefaultCloseOperation(EXIT_ON_CLOSE);

    panel = new JPanel();
    inputBox = new JTextField();
    chatBox = new JTextArea();
    chatLabel = new JLabel("Chatroom");
    inputLabel = new JLabel("input");
    sendButton = new JButton("Send");

    sendButton.addActionListener(this);

    panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));

    panel.add(chatLabel);
    panel.add(chatBox);
    panel.add(inputLabel);
    panel.add(inputBox);
    panel.add(sendButton);

    add(panel);
    setVisible(true);
  }

  public void actionPerformed(ActionEvent ae){
    String e = inputBox.getText();
    inputBox.setText("");

    String whole = sender.username + ": " + e;

    String x = chatBox.getText();
    x = x + "\n" + whole;
    chatBox.setText(x);

    sender.sendMessage(whole);
  }
}
