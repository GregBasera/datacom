import java.io.*; //BufferedReader, PrintWriter, InputStreamReader
import java.util.*; //Scanner

public class Receiver implements Runnable{
	private BufferedReader sin;
	private Chat w;

	public Receiver(BufferedReader sin, Chat w){
		this.sin = sin;
		this.w = w;
	}

	public void run(){
		String msg;

		try{
			while(true){
				while((msg = sin.readLine()) == null){}
				System.out.print("\n" + msg + "\n> ");
				String e = w.chatBox.getText();
				e = e + "\n" + msg;
				w.chatBox.setText(e);
			}
		}catch(Exception e){}
	}
}
